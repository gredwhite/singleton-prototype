package com.example.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by redwhite on 25.10.2015.
 */
@Service
public class ValidatorHolder {
    @Autowired
    private MyValidator myValidator;

    public MyValidator getMyValidator() {
        return myValidator;
    }
    /*
    public void setMyValidator(MyValidator myValidator) {
        this.myValidator = myValidator;
    }*/
}
