package com.example;

import com.example.domain.MyValidator;
import com.example.domain.ValidatorHolder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@Configuration
@ComponentScan("com.example.domain")
public class DemoApplication {


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        ApplicationContext context = new AnnotationConfigApplicationContext(DemoApplication.class);
        ValidatorHolder validatorHolder1 = (ValidatorHolder) context.getBean("validatorHolder");
        ValidatorHolder validatorHolder2 = (ValidatorHolder) context.getBean("validatorHolder");
        System.out.println(validatorHolder1.getMyValidator() == validatorHolder1.getMyValidator());
        System.out.println(validatorHolder1.getMyValidator().getObject() == validatorHolder1.getMyValidator().getObject());
        System.out.println("+++++++++++");
        System.out.println(validatorHolder1 == validatorHolder2);
        MyValidator myValidator = validatorHolder1.getMyValidator();
        System.out.println(myValidator.getObject() == myValidator.getObject());

        System.out.println("=====================================");
        System.out.println(validatorHolder1.getMyValidator() == validatorHolder2.getMyValidator());
        System.out.println(validatorHolder1.getMyValidator().getObject() == validatorHolder2.getMyValidator().getObject());
        System.out.println("=====================================");

        /*ApplicationContext xmlContext = new FileSystemXmlApplicationContext("classpath:applicationContext.xml");
          ValidatorHolder validatorHolder21 = (ValidatorHolder) xmlContext.getBean("validatorHolder");
        ValidatorHolder validatorHolder22 = (ValidatorHolder) xmlContext.getBean("validatorHolder");
        MyValidator myValidator = (MyValidator) xmlContext.getBean("validator");
        System.out.println("=====================================");
        //     System.out.println(validatorHolder21.getMyValidator() == validatorHolder22.getMyValidator());
        System.out.println("=====================================");
        validatorHolder22.getMyValidator().getObject(); */
    }
}
